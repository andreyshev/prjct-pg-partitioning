package test

import (
	"database/sql"
	"fmt"
	"math/rand"
	"testing"
	"time"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

func benchmarkExecute(port int, execFunc execFunc, b *testing.B) {
	rand.NewSource(time.Now().UnixNano())
	b.SetParallelism(50)
	db, err := connection(port)
	if err != nil {
		b.Error(err)
		return
	}
	for n := 0; n < b.N; n++ {
		if err := execFunc(db); err != nil {
			b.Error(err)
			return
		}
	}
}

func BenchmarkInsertPartitioning(b *testing.B) { benchmarkExecute(port_partitioning, insertUser, b) }
func BenchmarkInsertDefault(b *testing.B)      { benchmarkExecute(port_default, insertUser, b) }
func BenchmarkSelectPartitioning(b *testing.B) {
	benchmarkExecute(port_partitioning, selectUserByCountry, b)
}
func BenchmarkSelectDefault(b *testing.B) { benchmarkExecute(port_default, selectUserByCountry, b) }
func BenchmarkDeletePartitioning(b *testing.B) {
	benchmarkExecute(port_partitioning, deleteRandUser, b)
}
func BenchmarkDeleteDefault(b *testing.B) { benchmarkExecute(port_default, deleteRandUser, b) }

func TestInserts(t *testing.T) {
	t.Skip()
	rand.NewSource(time.Now().UnixNano())

	db, err := connection(port_partitioning)
	if err != nil {
		t.Error(err)
		return
	}
	for i := 0; i < 1000000; i++ {
		if err := insertUser(db); err != nil {
			t.Error(err)
			return
		}
	}

}

const (
	user              = "postgres"
	pass              = "postgres"
	host              = "127.0.0.1"
	db                = "user"
	port_partitioning = 5432
	port_default      = 6432
)

func connection(port int) (*sql.DB, error) {
	connStr := fmt.Sprintf("postgresql://%s:%s@%s:%d/%s?sslmode=disable", user, pass, host, port, db)
	// Connect to database
	return sql.Open("postgres", connStr)
}

var countries = []string{"UA", "US", "UK"}

type execFunc func(db *sql.DB) error

func insertUser(db *sql.DB) error {
	if _, err := db.Exec("INSERT INTO users VALUES($1,$2,$3,$4)", countries[randInt(0, 3)], uuid.New().String(), RandStringBytesMaskImpr(6), randInt(0, 10)); err != nil {
		return err
	}
	return nil
}

func selectUserByCountry(db *sql.DB) error {
	if _, err := db.Exec("SELECT * FROM users WHERE country_iso=$1", countries[randInt(0, 3)]); err != nil {
		return err
	}
	return nil
}

func deleteRandUser(db *sql.DB) error {
	if _, err := db.Exec("DELETE FROM users WHERE uuid in (SELECT uuid FROM users LIMIT 1)"); err != nil {
		return err
	}
	return nil
}

func randInt(min, max int) int {
	return rand.Intn(max-min) + min
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

func RandStringBytesMaskImpr(n int) string {
	b := make([]byte, n)
	// A rand.Int63() generates 63 random bits, enough for letterIdxMax letters!
	for i, cache, remain := n-1, rand.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}
