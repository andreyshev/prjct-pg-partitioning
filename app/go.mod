module app

go 1.18

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/pkg/errors v0.9.1
)

require github.com/lib/pq v1.10.7 // indirect
