*Vertical sharding (partitioning)*

Setup db1:
```
CREATE DATABASE "user";

CREATE TABLE users (
country_iso        varchar(2) not null,
uuid               varchar(36),
rand_seed          text,
rand_int           int
) PARTITION BY LIST (country_iso);

create table user_part_ua
partition of users
for values in ('UA');

create table user_part_us
partition of users
for values in ('US');

create table user_part_uk
partition of users
for values in ('UK');
```

Setup db2

```
CREATE DATABASE "user";

CREATE TABLE users (
country_iso        varchar(2) not null,
uuid               varchar(36),
rand_seed          text,
rand_int           int
);
```

Insert user data from users.csv

Benchmark results with partitioning and without (default)

```
BenchmarkInsertPartitioning-10              1574            744316 ns/op
BenchmarkInsertDefault-10                   1471            714060 ns/op
BenchmarkSelectPartitioning-10                 7         149301244 ns/op
BenchmarkSelectDefault-10                      7         168552720 ns/op
BenchmarkDeletePartitioning-10                14          76847580 ns/op
BenchmarkDeleteDefault-10                     16          68697141 ns/op
```

Conclusion

As we can see partitioning have overhead for delete/insert operation but a little bit faster select operations  